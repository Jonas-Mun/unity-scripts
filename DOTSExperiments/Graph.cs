﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Rendering;
using Unity.Mathematics;

public class Graph : MonoBehaviour
{
    [SerializeField] private Mesh mesh;
    [SerializeField] private Material material;

    [Range(10, 100)]
    public int resolution = 10;

    float scale = 1 / 5f;

    // Start is called before the first frame update
    void Start()
    {
        EntityManager entityManager = World.Active.EntityManager;

        EntityArchetype entityArchetype = entityManager.CreateArchetype(
            typeof(Translation),
            typeof(Scale),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(GraphFunction)
            );

        NativeArray<Entity> entityArray = new NativeArray<Entity>(resolution, Allocator.Temp);
        entityManager.CreateEntity(entityArchetype, entityArray);

        float step = 2f / resolution;
        float scale = 1 * step;

        for (int i = 0; i < resolution; i++) 
        {
            Entity entity = entityArray[i];


            // Set Components
            float positionX = (i + 0.5f) * step - 1f;


            entityManager.SetComponentData(entity, new Translation {
                Value = new float3(1 * positionX, 0f, 0f)
            });

            entityManager.SetComponentData(entity, new Scale {
                Value = scale
            });

            entityManager.SetComponentData(entity, new GraphFunction
            {
                function = 0
            });

            entityManager.SetSharedComponentData(entity, new RenderMesh {
                mesh = mesh,
                material = material
            });

        }

    }

    void Update()
    {
       
        
    }

    
}
