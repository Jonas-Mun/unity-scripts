﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;





public class GraphFunctionSystem : ComponentSystem
{
    enum FunctionName { Sine };

    delegate float Function(float x, float t);
    Function[] functions = {
        Sine
        };

    protected override void OnUpdate()
    {

        float t = Time.time;
        Entities.ForEach(
            (ref Translation translation, ref GraphFunction func) =>
        {
            Function f = functions[func.function];
            float y = f(translation.Value.x, Time.time);


            translation.Value.y = y;
        });
    }

    static private float Sine(float x, float t)
    {
        float y = math.sin(math.PI * (x + t));

        return y;
    }



    
}
