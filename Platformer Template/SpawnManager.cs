﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject player;
    public GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnObjects()
    {
        GameObject playerFab = Instantiate(player, player.transform.position, player.transform.rotation);
        SpawnEnemy(playerFab);
    }

    private void SpawnEnemy(GameObject objectToFollow)
    {
        GameObject enemFab = Instantiate(enemy, enemy.transform.position, enemy.transform.rotation);
        enemFab.GetComponent<MoveTowards>().towards = objectToFollow.GetComponent<Transform>();
    }
}
