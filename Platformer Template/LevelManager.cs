﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public GameObject levelPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void buildLevel()
    {
        Instantiate(levelPrefab, levelPrefab.transform.position, levelPrefab.transform.rotation);
    }
}
