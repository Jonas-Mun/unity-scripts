﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{

    public Mesh mesh;
    public float scale = 1f;

    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<Transform>().localScale *= scale;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
