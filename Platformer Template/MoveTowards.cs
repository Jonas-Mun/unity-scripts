﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour
{
    public Transform towards;

    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        speed = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = towards.position - transform.position;
        transform.Translate(direction.normalized * Time.deltaTime * speed);

    }
}
