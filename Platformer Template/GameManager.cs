﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public LevelManager levelManager;
    public SpawnManager spawnManager;
    // Start is called before the first frame update
    void Start()
    {
        levelManager.buildLevel();
        spawnManager.SpawnObjects();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
