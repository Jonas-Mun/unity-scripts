﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody rb;

    private bool jumped;
    private bool halfJump;
    private bool onGround;

    private float jumpForce = 4f;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        // Set bools
        jumped = false;
        halfJump = false;
        onGround = false;
    }

    public float horizontalInput;
    

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        Vector3 direction = Vector3.right * horizontalInput;

        transform.Translate(direction * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.Space) && onGround)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            onGround = false;
            jumped = true;
        } else if (Input.GetKeyDown(KeyCode.Space) && jumped && !halfJump)
        {
            rb.AddForce(Vector3.up * (jumpForce), ForceMode.Impulse);
            halfJump = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            jumped = false;
            halfJump = false;
            onGround = true;
            Debug.Log("On Ground");
        }

        if (collision.gameObject.CompareTag("Deadly"))
        {
            Debug.Log("You Died");
        }
    }
}
