﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragFormation : MonoBehaviour
{
    

    float sizeOfUnit = 1;
    float distanceBetweenEachUnit = 1f;
    public bool dragging;
    public bool stop;
    public Vector3 initCoord;
    Vector3 endCoord;
    Vector3 currCoord;

    Vector3 endRadiusCoord;

    public GameObject guide;

    public float radius = 0;

    public List<Transform> liveUnits;

    public Camera mainCam;
    public LayerMask groundMask;

    public struct PositionFormationInfo
    {
        public float radius;
        public Vector3 init;
        public Vector3 end;
        public List<Transform> positions;
    };

    public PositionFormationInfo positionInfo;

    void Start()
    {
        liveUnits = new List<Transform>();
        
    }

    void Update()
    {
        
        if (dragging)
        {

            Vector3 currCoord = Utilities.ClickedCoord(mainCam, groundMask);
            RotateGuide();
            float newRadius = Utilities.CalculateRadius(initCoord, currCoord);
            if (newRadius != radius)
            {
                radius = newRadius;
                Debug.Log(radius);
                float numUnitsInLine = Mathf.Round(radius / sizeOfUnit);

                if (numUnitsInLine == 0)
                {
                    numUnitsInLine = 1;
                }

                SpawnUnits((int)numUnitsInLine, distanceBetweenEachUnit);
            }

        }
        if (dragging & stop)
        {
            dragging = false;
            HideDisplayUnits();
            CreatePositionInfo();
            // Acquire positions

            stop = false;
        }

        
    }

    private  void CreatePositionInfo()
    {
        List<Transform> positions = new List<Transform>();
        for (int i = 0; i < liveUnits.Count; i++)
        {
            Transform currPosition = liveUnits[i];
            positions.Add(currPosition);
        }

        positionInfo = new PositionFormationInfo();
        positionInfo.radius = radius;
        positionInfo.init = initCoord;
        positionInfo.end = endRadiusCoord;
        positionInfo.positions = positions;

       
    }

    public PositionFormationInfo GetPositionInfo()
    {
        return positionInfo;
    }

    private void HideDisplayUnits()
    {
        for (int i = 0; i < liveUnits.Count; i++)
        {
            liveUnits[i].gameObject.SetActive(false);
        }
    }

    private void MouseDragging()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Beginning to Drag");
            dragging = true;
            initCoord = Utilities.ClickedCoord(mainCam, groundMask);

            guide.transform.position = initCoord;

            Debug.Log("Init Coordinate: " + initCoord);
        }
        if (Input.GetMouseButtonUp(1))
        {
            Debug.Log("Stopped dragging");
            dragging = false;
            endCoord = Utilities.ClickedCoord(mainCam, groundMask);



            float radius = Utilities.CalculateRadius(initCoord, endCoord);
            Debug.Log("Radius: " + radius);
            float numUnitsInLine = Mathf.Round(radius / sizeOfUnit);
            Debug.Log("Number of Units: " + numUnitsInLine);

            guide.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            
            //SpawnUnits((int)numUnitsInLine);


        }
    }

    public void AddUnits(int numUnitsToAdd, GameObject unitFab)
    {
        int length = liveUnits.Count;

        
            for (int i = 0; i < numUnitsToAdd; i++)
            {
                GameObject newUnit = Instantiate(unitFab);
                newUnit.SetActive(false);
                liveUnits.Add(newUnit.transform);
            }
        }



    private void SpawnUnits(int numUnits, float distanceBetweenUnits)
    {

        
        // Place Lead units on radius
        for (int i = 0; i < numUnits; i++)
        {
            float offsetForNextUnit = distanceBetweenUnits * i;

            Vector3 cosSin = Utilities.CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

            float xSpawnLoc = initCoord.x + (offsetForNextUnit * cosSin.x);
            float zSpawnLoc = initCoord.z + (offsetForNextUnit * -cosSin.y);

            Vector3 spawnLocation = new Vector3(xSpawnLoc, initCoord.y, zSpawnLoc);

            //Instantiate(unitFab, spawnLocation, unitFab.transform.rotation);
            liveUnits[i].position = spawnLocation;
            liveUnits[i].gameObject.SetActive(true);

            // Get coordinate position of the last unit in lead
            if (i == numUnits - 1)
            {
                endRadiusCoord = liveUnits[numUnits - 1].position;
                Debug.Log(endRadiusCoord);
            }

        }

        

        // Set units outside of radius to be be behind the lead.
        for (int i = 0; i < liveUnits.Count - numUnits; i++)
        {
            //liveUnits[(liveUnits.Count - i)-1].gameObject.SetActive(false);

            Vector3 cosSin = Utilities.CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

            Transform lead = liveUnits[i];

            Vector3 coordinateBehindLead = new Vector3(cosSin.y, 0f, cosSin.x);

            Transform follower = liveUnits[numUnits + i];
            follower.position = (lead.position - coordinateBehindLead);
            follower.gameObject.SetActive(true);


        }
        //guide.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
    }

   

    

    

    private void RotateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {

            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));


        }
    }
}
