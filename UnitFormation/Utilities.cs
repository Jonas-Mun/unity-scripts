﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    

    public static Vector3 ClickedCoord(Camera mainCam, LayerMask groundMask)
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;   //Coordinates of clicked position.

            Debug.Log(hit.point);
        }

        return clickedCoord;
    }

    public static Vector3 CosSinVector(float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle);
        float y = Mathf.Sin(Mathf.Deg2Rad * angle);

        Vector3 cosSinValues = new Vector3(x, y, 0f);

        return cosSinValues;
    }

    public static float CalculateRadius(Vector3 init, Vector3 end)
    {
        float xMagnitude = end.x - init.x;
        float zMagnitude = end.z - init.z;

        float radius = PythagoraTheorem(xMagnitude, zMagnitude);

        return radius;
    }

    public static float PythagoraTheorem(float x, float y)
    {

        return Mathf.Sqrt((x * x) + (y * y));
    }


}
