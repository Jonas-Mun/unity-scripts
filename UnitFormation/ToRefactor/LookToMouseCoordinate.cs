﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToMouseCoordinate : MonoBehaviour
{
    public Camera mainCamera;

    public GameObject parent;
    public GameObject unit;

    public LayerMask mask;

    //Unit
    float xPos = 1;
    float zPos = 1;

    float offset = 0;
    // Update is called once per frame
    void Update()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, mask))
        {
            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);

            // Rotate parent Object
            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }


    }
}
