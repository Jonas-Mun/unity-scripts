﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnLineOrientated : MonoBehaviour
{
    //How far away from the center. Unit Circle == 1
    float xPos = 1;
    float zPos = 1;

    float offset = 0;   // From center

    public GameObject parent;
    public GameObject unit;

    // Update is called once per frame
    void Update()
    {
        // Spawn Unit objects on a line from centre
        if (Input.GetKeyDown(KeyCode.Space))
        {

            Debug.Log(parent.transform.rotation.eulerAngles.y);

            // Cos/Sine coordinates
            float xPosCos = Mathf.Cos(Mathf.Deg2Rad * parent.transform.rotation.eulerAngles.y);
            float zPosSin = Mathf.Sin(Mathf.Deg2Rad * parent.transform.rotation.eulerAngles.y);

            Debug.Log("X " + xPosCos);
            Debug.Log("Z " + zPosSin);

            float xCoord = (xPosCos * xPos) * offset;   //Position from center 
            float zCoord = -(zPosSin * zPos) * offset;   //Position from center

            GameObject unitFab = Instantiate(unit, new Vector3(xCoord, 0f, zCoord), parent.transform.rotation);
            offset++;

            unitFab.transform.parent = parent.transform;

        }
    }
}
