﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationManager : MonoBehaviour
{
    public Camera mainCam;

    public GameObject unit;
    public GameObject mainFab;
    public LayerMask groundMask;

    public GameObject guide;    //Helps acquire rotation angle for units

    Vector3 initCoord;
    Vector3 currCoord;

    public float distanceBetweenUnits = 5;
    public float hypotenuse;

    private float numUnitsBetween;

    public float maxUnits;

    private bool isDrag;

    List<GameObject> liveUnits = new List<GameObject>();

    // Update is called once per frame
    void FixedUpdate()
    {
        MouseDragging();

        maxUnits = liveUnits.Count;

        if (isDrag)
        {
            OrientateGuide();
            currCoord = ClickedPoint();
            float newHypotenuse = CalculateMagnitude(initCoord, currCoord );
            
            // Check if size of line has changed
            if (newHypotenuse != hypotenuse)
            {
                hypotenuse = newHypotenuse;
                numUnitsBetween = Mathf.Round(hypotenuse / 1);
                // Change number of live units
                UpdateLiveUnitsCapacity((int)numUnitsBetween);
            }
            
            // Display live units
            UpdateUnitPosition((int)numUnitsBetween);
            
        }
    }

    private void UpdateLiveUnitsCapacity(int numUnitsBetween)
    {
        int length = liveUnits.Count;


        if (numUnitsBetween > length)
        {
            int difference = (int)numUnitsBetween - length;
            for (int i = 0; i < difference; i++)
            {
                GameObject newUnit = Instantiate(unit);
                
                newUnit.SetActive(false);
                liveUnits.Add(newUnit);
            }
        }
    }

    private void UpdateUnitPosition(int unitsBetween)
    {
        // Place Lead Units
        for (int i = 0; i < unitsBetween; i++)
        {
            Vector3 onHypotenuse = CoordinatesOnHypotenuse(i, distanceBetweenUnits);
            GameObject currentUnit = liveUnits[i];

            currentUnit.transform.Translate(onHypotenuse - currentUnit.transform.position);

            
            currentUnit.SetActive(true);
        }

        // Place Follow Units -> Can use Coordinates on Hypotenuse.
        float offsetFromLeadX = Mathf.Sin(Mathf.Deg2Rad * (guide.transform.rotation.eulerAngles.y - 90));
        float offsetFromLeadZ = Mathf.Cos(Mathf.Deg2Rad * (guide.transform.rotation.eulerAngles.y - 90));
        for  (int i = 0; i < (maxUnits - unitsBetween); i++)
        {
            

            Vector3 offsetFromLead = new Vector3(offsetFromLeadX, 0f, offsetFromLeadZ);
            Debug.Log("Offsets: " + offsetFromLead);

            GameObject follower = liveUnits[unitsBetween + i];  
            follower.transform.position = (liveUnits[i].transform.position - offsetFromLead);   // Place behind lead
            
            Debug.Log("New Position " + follower.transform.position);
            follower.SetActive(true);
        }

        /*
        int length = liveUnits.Count;
        for (int i = 0; i < length - unitsBetween; i++)
        {
            liveUnits[(length-1) - i].SetActive(false);
        }*/
    }

    private Vector3 CoordinatesOnHypotenuse(float offset, float distanceBetween)
    {
        // Distance from the center
        float nextUnitOffset = distanceBetween * offset;

        // Get orientated coordinates
        Vector3 orientatedCoords = OrientedCoordinates(nextUnitOffset);

        // Start from the initial coordinate
        float nextXCoord = orientatedCoords.x + initCoord.x;
        float nextZCoord = orientatedCoords.z + initCoord.z;

        float xOrientated = nextXCoord;    // Should work
        float zOrientated = nextZCoord;

        Vector3 spawn = new Vector3(xOrientated, initCoord.y, zOrientated);

        return spawn;
    }

    private void MouseDragging()
    {
        if (Input.GetMouseButtonDown(1))
        {
            initCoord = ClickedPoint();


            Vector3 guideDirection = initCoord - guide.transform.position;

            // guide.transform.Translate(guideDirection);
            guide.transform.position = initCoord;

            isDrag = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            isDrag = false;
            //PlaceUnit(distanceBetweenUnits, unit);
            PlaceMainUnit();

            // Reset guide orientation -> Prevents it going to random places after initial drag
            guide.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            
        }
    }

    // Gets the coordinate of the mouse click
    private Vector3 ClickedPoint()
    {
        // Get initial point of click
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;
            // Do Fancy calculations
        }

        

        return clickedCoord;
    }


    private void PlaceMainUnit()
    {
        int length = liveUnits.Count;

        for (int i = 0; i < length; i++)
        {
            Instantiate(mainFab, liveUnits[i].transform.position, liveUnits[i].transform.rotation);
        }
    }

    private void PlaceUnit(float distanceBetween, GameObject prefab)
    {

        //Debug.Log("Current Coordinate: " + currCoord.x);
        //Debug.Log("Initial Coordinate: " + initCoord.x);

        float sizeUnit = 1;

        // How many units fit from the initial coord to the last point clicked
        float numUnitsBetween = Mathf.Round(hypotenuse / sizeUnit);

        //Debug.Log("Number of Units" + numUnitsBetween );


        // Spawn Units
        for (int i = 0; i < numUnitsBetween; i++)
        {

            Vector3 spawn = CoordinatesOnHypotenuse(i, distanceBetween);
            GameObject unitFab = Instantiate(prefab, spawn, prefab.transform.rotation);
            //unitFab.transform.parent = guide.transform;
        }
    }

    private Vector3 OrientedCoordinates(float offset)
    {
        // Far away from center - Unit Circle
        float xUnit = 1;
        float zUnit = 1;

        //Debug.Log("## ORIENTATED COORDINATES - FUNC ##");

        // Cos/Sine coordinates
        float xPosCos = Mathf.Cos(Mathf.Deg2Rad * (guide.transform.rotation.eulerAngles.y - 90));
        float zPosSin = Mathf.Sin(Mathf.Deg2Rad * (guide.transform.rotation.eulerAngles.y - 90));

        //Debug.Log("Guide angle: " + guide.transform.rotation.eulerAngles.y);

        //Debug.Log("X Cos: " + xPosCos);
        //Debug.Log("Z Sin: " + zPosSin);

        float xCoord = (xPosCos * xUnit) * offset;   //Position from center 
        float zCoord = -(zPosSin * zUnit) * offset;   //Position from center

        //Debug.Log("X Coord * offset: " + xCoord);
        //Debug.Log("Z Coord * offset: " + zCoord);

        Vector3 orientedCoords = new Vector3(xCoord, 0f, zCoord);

        //Debug.Log("##------ END OF FUNC ------##");

        return orientedCoords;
    }

    private void OrientateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {

            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));


        }
    }

    private float CalculateMagnitude(Vector3 init, Vector3 current)
    {
        float magnitudeX;
        float magnitudeZ;


        if (current.x < init.x)
        {
            magnitudeX = init.x - current.x;
        }
        else
        {
            magnitudeX = current.x - init.x;
        }

        if (current.z < init.z)
        {
            magnitudeZ = init.z - current.z;
        }
        else
        {
            magnitudeZ = current.z - init.z;
        }

        float magnitude = CalculateHypotenuse(magnitudeX, magnitudeZ);

        return magnitude;
    }

    private float CalculateHypotenuse(float x, float y)
    {
        float hypotenuse = Mathf.Sqrt((x * x) + (y * y));
        return hypotenuse;
    }
}
