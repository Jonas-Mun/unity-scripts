﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceUnitsHorizontal : MonoBehaviour
{
    public Camera cam;
    public GameObject unit;     // What to display.

    public Vector3 initCoord;
    public Vector3 currCoord;

    public Vector3 lastCoord;

    public Vector3 placedUnitCoord;  // Coordinate where the latest unit was placed

    public bool isDragging;

    public LayerMask clickMask;

    public float distanceBetween = 1;   // Distance from first unit to next unit
    private int distanceIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        isDragging = false;
    }

    // Update is called once per frame
    void Update()
    {
        MouseDragging();    // Mouse dragging

        if (isDragging)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f, clickMask))
            {
                //Debug.Log("Coordinates Clicked: " + hit.point);
                //Debug.Log(Rotationhit.transform.rotation);
                currCoord = hit.point;
                //PlaceUnit();
                PlaceUnitV2();


                // Do Fancy calculations
            }
        }
        placedUnitCoord.x = Vector3.zero.x;   // Reset

        // Display unit if at an appropriate distance
        //
    }

    private void PlaceUnit()
    {
        float nextUnitPos = initCoord.x + (distanceBetween * distanceIndex);
        if (currCoord.x >= nextUnitPos)
        {
            Instantiate(unit, currCoord, unit.transform.rotation);
            placedUnitCoord.x = currCoord.x;
            distanceIndex++;
        }


        // Check if 
    }

    private void PlaceUnitV2()
    {
        float magnitudeX = currCoord.x - initCoord.x;

        float sizeUnit = 1;

        float numUnitsBetween = Mathf.Round(magnitudeX / sizeUnit);


        for (int i = 0; i < numUnitsBetween; i++)
        {
            float nextUnitPos = distanceBetween * i;
            Vector3 spawn = new Vector3(initCoord.x + nextUnitPos, initCoord.y, initCoord.z);
            Instantiate(unit, spawn, unit.transform.rotation);
        }
    }

    private void MouseDragging()
    {
        if (Input.GetMouseButtonDown(1))
        {
            // Get initial point of click
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100.0f, clickMask))
            {
                initCoord = hit.point;

                // Do Fancy calculations
            }


            isDragging = true;
            distanceIndex = 0;

        }

        if (Input.GetMouseButtonUp(1))
        {
            isDragging = false;
            //PlaceUnitV2();
        }
    }
}


