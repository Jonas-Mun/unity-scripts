﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Article : MonoBehaviour
{
    

    float sizeOfUnit = 1;
    private bool dragging;
    Vector3 initCoord;
    Vector3 endCoord;
    Vector3 currCoord;

    public Camera mainCam;
    public LayerMask groundMask;
    public GameObject unitFab;
    public GameObject guide;

    private float radius = 0;

    private List<Transform> liveUnits;

    void Start()
    {
        liveUnits = new List<Transform>();
    }

    void Update()
    {
        MouseDragging();
        if (dragging)
        {

            Vector3 currCoord = ClickedCoord();
            RotateGuide();
            float newRadius = CalculateRadius(initCoord, currCoord);
            if (newRadius != radius)
            {
                radius = newRadius;
                
                float numUnitsInLine = Mathf.Round(radius / sizeOfUnit);

                // Always maintain a unity on  display
                if (numUnitsInLine == 0)
                {
                    numUnitsInLine = 1;
                }

                StoreUnits((int)numUnitsInLine);
                SpawnUnits((int)numUnitsInLine);
            }
            

        }
    }

    private void MouseDragging()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Beginning to Drag");
            dragging = true;
            initCoord = ClickedCoord();

            guide.transform.position = initCoord;

            Debug.Log("Init Coordinate: " + initCoord);
        }
        if (Input.GetMouseButtonUp(1))
        {
            Debug.Log("Stopped dragging");
            dragging = false;
            endCoord = ClickedCoord();
    


            float radius = CalculateRadius(initCoord, endCoord);
            Debug.Log("Radius: " + radius);
            float numUnitsInLine = Mathf.Round(radius / sizeOfUnit);
            Debug.Log("Number of Units: " + numUnitsInLine);

            guide.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            //SpawnUnits((int)numUnitsInLine);


        }
    }

    private void StoreUnits(int numUnits)
    {
        int length = liveUnits.Count;

        if (numUnits > length)
        {
            // Calculate how many units to add
            int unitsToAdd = numUnits - length;
            for (int i = 0; i < unitsToAdd; i++)
            {
                GameObject newUnit = Instantiate(unitFab);
                newUnit.SetActive(false);
                liveUnits.Add(newUnit.transform);
            }
        }
    }

    

    private void SpawnUnits(int numUnits)
    {

        float distanceBetweenEachUnit = 1f;

        for (int i = 0; i < numUnits; i++)
        {
            float offsetForNextUnit = distanceBetweenEachUnit * i;

            Vector3 cosSin = CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

            float xSpawnLoc = initCoord.x + (offsetForNextUnit * cosSin.x);
            float zSpawnLoc = initCoord.z + (offsetForNextUnit * -cosSin.y);

            Vector3 spawnLocation = new Vector3(xSpawnLoc, initCoord.y, zSpawnLoc);

            //Instantiate(unitFab, spawnLocation, unitFab.transform.rotation);
            liveUnits[i].position = spawnLocation;
            liveUnits[i].gameObject.SetActive(true);

        }

        // Set units outside of radius to be inactive.
        for (int i = 0; i < liveUnits.Count - numUnits; i++)
        {
            //liveUnits[(liveUnits.Count - i)-1].gameObject.SetActive(false);

            Vector3 cosSin = CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

            Transform lead = liveUnits[i];

            Vector3 coordinateBehindLead = new Vector3(cosSin.y, 0f, cosSin.x);

            Transform follower = liveUnits[numUnits + i];
            follower.position = (lead.position - coordinateBehindLead);
            follower.gameObject.SetActive(true);


        }
        //guide.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
    }

    private Vector3 CosSinVector(float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle);
        float y = Mathf.Sin(Mathf.Deg2Rad * angle);

        Vector3 cosSinValues = new Vector3(x, y, 0f);

        return cosSinValues;
    }

    private float CalculateRadius(Vector3 init, Vector3 end)
    {
        float xMagnitude = end.x - init.x;
        float zMagnitude = end.z - init.z;

        float radius = PythagoraTheorem(xMagnitude, zMagnitude);

        return radius;
    }

    private float PythagoraTheorem(float x, float y)
    {

        return Mathf.Sqrt((x * x) + (y * y));
    }

    private Vector3 ClickedCoord()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;   //Coordinates of clicked position.

            Debug.Log(hit.point);
        }

        return clickedCoord;
    }

    private void RotateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {

            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));


        }
    }


}
