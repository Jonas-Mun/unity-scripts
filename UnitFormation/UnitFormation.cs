﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFormation : MonoBehaviour
{
    public Camera mainCam;
    public LayerMask groundMask;
    public GameObject unitFab;

    public GameObject unitSoldier;

    public int totalUnits;

    public List<GameObject> unitsAlive;
    // Start is called before the first frame update
    public DragFormation dragBehaviour;

    private bool waitingForPositions;

    public void Start()
    {
        //Testing purposes
        totalUnits = 10;

        dragBehaviour = GetComponent<DragFormation>();
        dragBehaviour.AddUnits(totalUnits, unitFab);
        dragBehaviour.mainCam = mainCam;
        dragBehaviour.groundMask = groundMask;

        unitsAlive = new List<GameObject>();

        StartUpUnits();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            dragBehaviour.initCoord = Utilities.ClickedCoord(mainCam, groundMask);
            dragBehaviour.guide.transform.position = dragBehaviour.initCoord;
            dragBehaviour.dragging = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            dragBehaviour.stop = true;
            waitingForPositions = true;
            
        }

        if (waitingForPositions && !dragBehaviour.stop)
        {

            // Acquire new positions
            PlaceUnits(dragBehaviour.positionInfo.positions);
            waitingForPositions = false;
        }

        if (Input.GetMouseButtonDown(0))
        {

            MoveUnits(Utilities.ClickedCoord(mainCam, groundMask), dragBehaviour.positionInfo.init, dragBehaviour.positionInfo.end, dragBehaviour.positionInfo.radius, unitsAlive);
        }
    }

    private void StartUpUnits()
    {
        for (int i = 0; i < totalUnits; i++)
        {
            GameObject newUnit = Instantiate(unitSoldier);
            
            newUnit.SetActive(false);
            unitsAlive.Add(newUnit);
        }
    }

    public void PlaceUnits(List<Transform> liveUnits)
    {
        for(int i = 0; i < totalUnits; i++)
        {
            List<Transform> formationPositions = liveUnits;
            unitsAlive[i].transform.position = formationPositions[i].position;
            unitsAlive[i].transform.gameObject.SetActive(true);
        }
    }

    public void MoveUnits(Vector3 pointToMove, Vector3 initCoord, Vector3 endCoord, float radius, List<GameObject> unitsToMove)
    {
        
        Vector3 midPoint = (endCoord - initCoord) / 2;
        Vector3 midPointCoordinate = midPoint + initCoord;

        Debug.Log("Mid Point of formation: " + midPointCoordinate);

        Vector3 deltaCoordinate = pointToMove - midPointCoordinate;
        float deltaX = pointToMove.x - midPoint.x;
        float deltaZ = pointToMove.z - midPoint.z;

        Debug.Log("Point To Move: " + pointToMove);
        Debug.Log("Delta Coordinates: " + deltaCoordinate);

        dragBehaviour.positionInfo.init = dragBehaviour.positionInfo.init + deltaCoordinate;
        dragBehaviour.positionInfo.end = dragBehaviour.positionInfo.end + deltaCoordinate;

        for (int i = 0; i < unitsToMove.Count; i++)
        {
            unitsToMove[i].transform.position = unitsToMove[i].transform.position + deltaCoordinate;
        }
    }

    
}
