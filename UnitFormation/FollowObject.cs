﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField] private float radius = 5;
    public GameObject Lead;

    // Update is called once per frame
    void Update()
    {
        //Convert rotation of LEAD into euler angles
        float leadEulerAngle = Lead.transform.rotation.eulerAngles.y;

        // Base angle on the Unit Circle
        float nonNegativeAngle = ModulateAngle(leadEulerAngle) * -1;


        float angleBehind = BehindObjectAngle(nonNegativeAngle);

        // Convert Angle into Radians
        float radianBehind = Mathf.Deg2Rad * angleBehind;

        // Coordinates behind the LEAD based on Unit Circle - Offset
        Vector3 coordBehind = new Vector3(radius * Mathf.Cos(radianBehind), 0, radius * Mathf.Sin(radianBehind));
        Debug.Log(radius);

        transform.position = Lead.transform.position + coordBehind;
    }

    private float ModulateAngle(float angle)
    {
        return angle % 360;
    }

    private float BehindObjectAngle(float angle)
    {
        float behind = 180 + 90;
        return angle + behind;
    }
}
