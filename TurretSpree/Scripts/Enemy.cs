﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform towards;

    Vector3 movement;

    private void Update()
    {
        movement = (towards.transform.position - transform.position).normalized;
        transform.Translate(movement * 0.05f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("DamageArea"))
        {
            Destroy(gameObject);
        }

    }
}
