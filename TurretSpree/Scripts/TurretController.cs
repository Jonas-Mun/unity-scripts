﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    [SerializeField]

    public StateMachine stateMachine;

    public Transform turretHead;
    public Transform crosshair;

    public Camera cam;

    public ShootRay shootingRay;

    float speedRotation = 1f;

    private void Start()
    {
        shootingRay = GetComponentInChildren<ShootRay>();
        stateMachine.ChangeState(new ShootingState(this, shootingRay));
    }

    void Update()
    {
        stateMachine.currentState.Update();
    }

    public void ChangeState(IState newState)
    {
        stateMachine.ChangeState(newState);
    }
    
}
