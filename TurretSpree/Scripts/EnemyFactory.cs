﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyFactory : ScriptableObject
{

    public Enemy prefab;

    public Enemy Get()
    {
        Enemy instance = Instantiate(prefab);

        return instance;
    }
}
