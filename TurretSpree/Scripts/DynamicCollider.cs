﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCollider : MonoBehaviour
{
    BoxCollider boxCollider;

    private float effectColliderDuration = 4.0f;
    private bool colliderActive;
    // Start is called before the first frame update
    void Start()
    {
        boxCollider = GetComponentInChildren<BoxCollider>();
        colliderActive = false;
        //boxCollider.gameObject.SetActive(false);
    }

    public void UseDynamicCollider(Vector3 initCoord, Vector3 endCoord)
    {
        PlaceCollider(initCoord, endCoord);
        StartCoroutine(ActivateCollider());
    }

    private void PlaceBetweenEnds(Vector3 initCoord, Vector3 endCoord)
    {
        Vector3 direction = endCoord - initCoord;
        Vector3 midPoint = direction / 2;

        Debug.Log("Mid Point: " + midPoint);

        boxCollider.gameObject.transform.position = initCoord + midPoint;
        //transform.position = initCoord + midPoint;
    }

    private void ScaleCollider(Vector3 initCoord, Vector3 endCoord)
    {
        Vector3 direction = endCoord - initCoord;
         float magnitude = direction.magnitude;
        
        boxCollider.size = new Vector3((magnitude), 1.0f, 1.0f);
    }

    private void OrientCollider(Vector3 initCoord, Vector3 endCoord)
    {
        Vector3 deltaCoord = endCoord - initCoord;

        float angle = Mathf.Atan2(-deltaCoord.z, deltaCoord.x) * Mathf.Rad2Deg;
        if (angle < 0)
        {
            angle = angle + 360;
        }

        Vector3 rotationVector = new Vector3(transform.localEulerAngles.x, angle, transform.localEulerAngles.z);
        Debug.Log(rotationVector);

        boxCollider.gameObject.transform.rotation = Quaternion.Euler(rotationVector);
        //transform.rotation = Quaternion.Euler(rotationVector);
    }

    private void PlaceCollider(Vector3 initCoord, Vector3 endCoord)
    {
        PlaceBetweenEnds(initCoord, endCoord);
        OrientCollider(initCoord, endCoord);
        ScaleCollider(initCoord, endCoord);
        
    }

    private IEnumerator ActivateCollider()
    {
        boxCollider.gameObject.SetActive(true);

        yield return new WaitForSeconds(effectColliderDuration);

        boxCollider.gameObject.SetActive(false);
        colliderActive = false;
    }
}
