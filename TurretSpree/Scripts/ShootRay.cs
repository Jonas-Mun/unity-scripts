﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootRay : MonoBehaviour
{

    public float weaponRange;
    public float damage;

    public float fireRate = 0.25f;
    public float nextFire;

    public Camera fpsCam;

    public Transform gunEndLeft;
    public Transform gunEndRight;

    private Transform gunEnd;

    AudioSource gunAudio;
    LineRenderer laserLine;

    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);

    private bool rightEnd;

    

    // Start is called before the first frame update
    void Start()
    {
        //fpsCam = GetComponentInParent<Camera>();
        gunAudio = GetComponent<AudioSource>();
        laserLine = GetComponent<LineRenderer>();
        rightEnd = true;

        
    }

    public void ShootRayMode()
    {
        Ray rayOrigin = fpsCam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(rayOrigin.origin, rayOrigin.direction * 10, Color.yellow);
        RaycastHit hit;
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {

            gunEnd = ToggleGunEnd();

            laserLine.SetPosition(0, gunEnd.position);


            if (Physics.Raycast(rayOrigin.origin, rayOrigin.direction, out hit, weaponRange))
            {
                laserLine.SetPosition(1, hit.point);
                DealEffect(hit.collider);


            }
            else
            {
                laserLine.SetPosition(1, rayOrigin.direction * weaponRange);
            }

            nextFire = Time.time + fireRate;
            StartCoroutine(ShotEffect());
        }
    }
    // Update is called once per frame
    
    IEnumerator ShotEffect()
    {
        gunAudio.Play();
        laserLine.enabled = true;

        yield return shotDuration;
        laserLine.enabled = false;
        
    }

    private Transform ToggleGunEnd()
    {
        if (rightEnd)
        {
            
            rightEnd = !rightEnd;
            return gunEndRight;
        }
        else
        {
            
            rightEnd = !rightEnd;
            return gunEndLeft;
        }
    }

    private void DealEffect(Collider obj)
    {
        Destroy(obj.gameObject);
    }
}
