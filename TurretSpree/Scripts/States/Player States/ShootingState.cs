﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingState : IState
{
    TurretController turret;
    ShootRay shootingRay;

    public ShootingState(TurretController turret, ShootRay shootingRay)
    {
        this.turret = turret;
        this.shootingRay = shootingRay;
    }

    public void Update()
    {
       
        var mousePosition = Input.mousePosition;
        turret.turretHead.LookAt(turret.cam.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, -100f)));
        Ray ray = turret.cam.ScreenPointToRay(Input.mousePosition);

        Vector3 crosshair_offset = new Vector3(ray.origin.x, ray.origin.y, ray.origin.z * -1f);
        turret.crosshair.transform.position = Input.mousePosition;

        shootingRay.ShootRayMode();
    }

    public void Exit()
    {
        turret.crosshair.gameObject.SetActive(false);
    }

    public void Enter()
    {
        turret.crosshair.gameObject.SetActive(true);
    }
}
