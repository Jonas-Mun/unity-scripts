﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopViewState : IState
{
    TurretController turret;

    [SerializeField]
    LayerMask groundMask;

    DynamicCollider areaEffect;

    Camera topCam;
    private Vector3 initCoord;
    private Vector3 endCoord;
    private int mouseClick = 1;

    LineRenderer laserLine;

    public TopViewState(TurretController turret, LayerMask groundMask, Camera cam)
    {
        this.topCam = cam;
        this.groundMask = groundMask;
        this.turret = turret;
        laserLine = turret.turretHead.GetComponent<LineRenderer>();
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(mouseClick))
        {
            laserLine.enabled = false;
            initCoord = ClickedCoord();
        }
        if (Input.GetMouseButtonUp(mouseClick))
        {
            // Register end coord
            endCoord = ClickedCoord();
            // Line Render
            laserLine.SetPosition(0, initCoord);
            laserLine.SetPosition(1, endCoord);
            laserLine.enabled = true;
        }
    }

    public void Exit()
    {
        laserLine.enabled = false;
    }

    public void Enter()
    {

    }

    private Vector3 ClickedCoord()
    {
        Ray ray = topCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;
        }
        return clickedCoord;
    }
}
