﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestState : IState
{
    TurretController turret;

    public TestState(TurretController turret)
    {
        this.turret = turret;
    }

    public void Update()
    {
        Debug.Log("Test State");
    }

    public void Exit()
    {
        Debug.Log("Left mouse event");
    }

    public void Enter()
    {
        Debug.Log("Mouse movement event");
    }
}
