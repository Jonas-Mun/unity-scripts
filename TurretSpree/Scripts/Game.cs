﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public KeyCode createEnemyKey = KeyCode.E;
    public Transform enemyPrefab;

    public TurretController turretController;
    public DynamicCollider dynCollider;

    public Transform player;

    public EnemyFactory enemyFactory;
    public CameraManager cameraManager;

    public LayerMask groundMask;

    private float xBoundary = 10f;
    private float yHeight = 1.08f;
    private float zBoundSpawn = 20f;

    private float minZ = 5.0f;

    public KeyCode switchKey = KeyCode.O;

    private float createProgress;
    public float CreationSpeed { get; set; }

    public int currentState;

    private enum States { Shooting, Topview};

    private void Start()
    {
        cameraManager.SetUpCam(true, false);
        currentState = (int)States.Shooting;


    }

    private void Update()
    {
        if (Input.GetKeyDown(createEnemyKey))
        {
            CreateEnemy();
        } 

        // Change State
        if (Input.GetKeyDown(switchKey))
        {
            if (currentState == (int)States.Shooting)
            {
                turretController.stateMachine.ChangeState(new TopViewState(turretController, groundMask, cameraManager.topView, dynCollider));
                cameraManager.SwitchCam();
                currentState = (int)States.Topview;
            } else
            {
                turretController.stateMachine.ChangeState(new ShootingState(turretController, turretController.shootingRay));
                cameraManager.SwitchCam();
                currentState = (int)States.Shooting;
            }
        }

        // Spawn Enemies
        createProgress += Time.deltaTime * CreationSpeed;

        if (createProgress > 1.5f)
        {
            while (createProgress >= 1.5f)
            {
                CreateEnemy();
                createProgress -= 1.5f;
            }
        }
    }

    void CreateEnemy()
    {
        Enemy enemy = enemyFactory.Get();
        enemy.gameObject.transform.localPosition = RandomEnemyPosition();
        enemy.towards = player;
    }

    Vector3 RandomEnemyPosition()
    {
        Vector3 EnemyPos = new Vector3(Random.Range(-xBoundary, xBoundary), yHeight, Random.Range(minZ, zBoundSpawn));

        return EnemyPos;
    }
}
