﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera turretView;
    public Camera topView;

    

    private void Start()
    {
        turretView.gameObject.SetActive(true);
        topView.gameObject.SetActive(false);
    }

    public void SetUpCam(bool cam1, bool cam2)
    {
        turretView.gameObject.SetActive(cam1);
        topView.gameObject.SetActive(cam2);
    }

    public void SwitchCam()
    {
        

        turretView.gameObject.SetActive(!turretView.gameObject.activeSelf);
        topView.gameObject.SetActive(!topView.gameObject.activeSelf);
    }
}
